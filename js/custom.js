var interfeis_var = {
	"themeurl":"http:\/\/html.novarostudio.com\/flip\/",
	"slidereffect":"fade",
	"slider_interval":"100000",
	"slider_disable_nav":"0",
	"slider_disable_prevnext":"0"
};

jQuery(document).ready(function(){
	
	/*Add Class Js to html*/
	jQuery('html').addClass('js');	
	
	show_tab();
	
	show_toggle();
	
	topsearch_effects();
	
	toggle_menu();
	
	portfolio_effect();
	
	portfolioajax();
	
	show_socialicon();
	
	show_lightbox();
	
	form_styling();
	
	fullwidthwrap();
	
	appear_effect();
	
	counter_effect();
	
	footer_effect();
	
	loading_process();
	
	calculate_videoheight();
	
	if(jQuery('body').hasClass('nvrflippage')){
		Page.init();
	}
	
	change_uberOrientation();

});

jQuery(window).load(function(){
	header_effect();
	
	show_menu();
	
	if(jQuery('body').hasClass('nvrflippage')==false){
		show_carousel();
	}
	
	loader_effect();
	
	isotopeinit();
	
	slider_init();
	
	calculate_sliderheight();
	
	parallax_effect();
	
	scrollSpy();
});

jQuery(window).resize(function(){
	isotopeinit();
	
	fullwidthwrap();
	
	footer_effect();
	
	calculate_videoheight();
	
	change_uberOrientation();
	
	calculate_sliderheight();
	
	jQuery('ul.topnav').css('top','');
});

jQuery(window).scroll(function(evt){
	scrollSpy();
});

function isMobile(){
	"use strict";
	var onMobile = false;
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) { onMobile = true; }
	return onMobile;
}

function loading_process(){
	"use strict";
	
	var loadedimages = 0;
	var loadedpercent = 0;
	
	jQuery('body').imagesLoaded()
	.always( function( instance ) { console.log('loading images'); })
	.fail( function() { console.log('all images loaded, at least one is broken'); })
	.done( function( instance ) {
		
	})
	.progress( function( instance, image ) {
		var totalimage = instance.images.length;
		var result = image.isLoaded ? 'loaded' : 'broken';
		if(result=='loaded'){
			loadedimages++;
		}
		loadedpercent = Math.round((loadedimages/totalimage)*100);
		jQuery('#loadingbar').css('width', loadedpercent+'%');
		console.log( 'image is ' + result + ' for ' + image.img.src );
	});
}

function loader_effect(){
	"use strict";
	
	jQuery("#loader").delay(500).fadeOut(); 
	jQuery(".loadercontainer").delay(1000).fadeOut("slow");
}

function header_effect(){
	"use strict";
	/*=================================== TOPSEARCH ==============================*/
	var outerheader = jQuery('#outerheader');
	var outerafterheader = jQuery('#outerafterheader');
	var outerslider = jQuery('#outerslider');
	var wpadminbar = jQuery('#wpadminbar');
	var sectiondef1 = jQuery('.nvronepage .sectionno-1 .section.default');
	var outermiddle = jQuery('#outermiddle');
	
	var adminbarinnerh = wpadminbar.innerHeight();
	var outerheaderinnerh = outerheader.innerHeight();
	var outerheadertop = outerheader.css("top");
	var windowheight = jQuery(window).height();

	var outerheaderoffset = 0;
	if(jQuery('body').hasClass('admin-bar')){
		outerheaderoffset = jQuery('#wpadminbar').innerHeight();
	}
	
	sectiondef1.css('padding-top', outerheaderinnerh);
	outerafterheader.css('padding-top', outerheaderinnerh);
	if(jQuery(window).width()<768){
		outermiddle.css('padding-top', outerheaderinnerh);
	}
	
	jQuery(window).scroll(function(evt){
		var scrolltop = jQuery(document).scrollTop();
		
		if(jQuery('body').hasClass('nvrlayout4')){
			if(scrolltop>headertextinnerh){
				outerheader.addClass("sticky");
				outerslider.addClass("sticky");
			}else{
				outerheader.removeClass("sticky");
				outerslider.removeClass("sticky");
			}
		}else{
			
			if(scrolltop>(outerheaderoffset)){
				outerheader.addClass("sticky");
			}else{
				outerheader.removeClass("sticky");
			}
		}
	});
}

function footer_effect(){
	jQuery('.nvrflippage #togglefoterwrapper').click(function(evt){
		
		var fwrapper = jQuery('.nvrflippage #footerwrapper');
		if(jQuery(window).width()<768){
			var outerfooterinnerH = jQuery('#outerfooter').innerHeight();
			var fwrapperH = outerfooterinnerH;
		}else{
			var outerfooterinnerH = jQuery('#outerfooter').innerHeight();
			var footersidebarinnerH = jQuery('#outerfootersidebar').innerHeight();
			var fwrapperH = outerfooterinnerH + footersidebarinnerH;
		}
		if(fwrapper.hasClass('show')){
			fwrapper.removeClass('show');
			fwrapper.css('height',0);
			jQuery(this).addClass('fa-angle-up').removeClass('fa-angle-down');
		}else{
			fwrapper.addClass('show');
			fwrapper.css('height',fwrapperH);
			jQuery(this).addClass('fa-angle-down').removeClass('fa-angle-up');
		}
		return false;
		
	});
	
}

function scrollSpy() {
	"use strict";
	var e, n, i, a, b, c;	
	var curSlide;
	var allnav = jQuery(".nvronepage ul.topnav li a");
	
	allnav.not('.external').each(function(){
		curSlide = jQuery( jQuery(this).attr("href") );
		
		if( curSlide.size() > 0 ){	
			a = curSlide.offset().top; 
			if(jQuery('body').hasClass('admin-bar')){
				a-=32;
			}
			b = jQuery(this);
			c = jQuery(window).scrollTop();
			
			if (a <= c) {
				allnav.parent().removeClass("current-menu-item");
				jQuery(this).parent().addClass("current-menu-item");
			} else {
				jQuery(this).parent().removeClass("current-menu-item");
			}
		}
	});
}

function parallax_effect(){
	if(!isMobile()){
		jQuery('.parallax').each(function(){
			jQuery(this).parallax("30%", 0.1);
		});
	}
}

function show_menu(){
	"use strict";
	/*=================================== MENU ===================================*/
    jQuery("ul.sf-menu").supersubs({ 
    minWidth		: 12,		/* requires em unit. */
    maxWidth		: 15,		/* requires em unit. */
    extraWidth		: 0	/* extra width can ensure lines don't sometimes turn over due to slight browser differences in how they round-off values */
                           /* due to slight rounding differences and font-family */
    }).superfish();  /* 
						call supersubs first, then superfish, so that subs are 
                        not display:none when measuring. Call before initialising 
                        containing tabs for same reason. 
					 */
	jQuery('.nvronepage ul.sf-menu a, .nvronepage ul.sf-menu li a, .nvronepage a.linked, .nvronepage a.sliderbutton').bind('click',function(event){
		var $anchor = jQuery(this);
		if(jQuery($anchor.attr('href')).length){
			var scrolltoppos = jQuery($anchor.attr('href')).offset().top;
	
			if(jQuery('body').hasClass('admin-bar')){
				var wpadminbar = jQuery('#wpadminbar');
				var adminbarinnerh = wpadminbar.innerHeight();
				scrolltoppos = scrolltoppos - adminbarinnerh;
			}
			
			if(!$anchor.hasClass('external')){
				jQuery('html, body').stop().animate({
				scrollTop: scrolltoppos
				}, 1500,'easeInOutExpo');
				
				event.preventDefault();
			}
		}
	});
}

function change_uberOrientation(){
	"use strict";
	var winwidth = jQuery(window).width();
	if(jQuery('body').hasClass('nvrlayout4')){
		if(winwidth>1023){
			jQuery('#megaMenu').addClass('megaMenuVertical').removeClass('megaMenuHorizontal');
		}else{
			jQuery('#megaMenu').addClass('megaMenuHorizontal').removeClass('megaMenuVertical');
		}
	}
}

/* !Fullwidth wrap for shortcodes & templates */
function fullwidthwrap(){
	"use strict";
	if( jQuery(".nvr-fullwidthwrap").length && jQuery(".nosidebar").length ){
		jQuery(".nvr-fullwidthwrap").each(function(){
			var $_this = jQuery(this),
				offset_wrap = $_this.position().left;

				var $offset_fs;
				var $scrollBar = 0;
			 	var containerwidth = jQuery('#outermain .container').width();
				var outerwidth = (jQuery('#outercontainer').width() - (jQuery('#outercontainer').width()%2));

				var paddingcol = parseInt(jQuery('.columns').css('padding-left'));

				if( jQuery('body').hasClass('boxed') ){
					$offset_fs = ((parseInt(outerwidth) - parseInt(containerwidth)) / 2);
				} else {
						var $windowWidth = (jQuery(window).width() <= parseInt(containerwidth)) ? parseInt(containerwidth) : jQuery(window).width();
						$offset_fs = Math.ceil( ((outerwidth + $scrollBar - parseInt(containerwidth)) / 2) );
				};
				$_this.css({
					width: outerwidth,
					"margin-left": -$offset_fs
				});
		});
	};
};

function show_tab(){
	"use strict";
	/*jQuery tab */
	var pathurl = window.location.href.split("#tab");
	var deftab = "";
	
	jQuery(".tab-content").hide(); /* Hide all content */

	if(pathurl.length>1){ 
		deftab = "#"+pathurl[1];
		var pdeftab = jQuery("ul.tabs li a[href="+deftab+"]").parent().addClass("active").show();
		var tabcondeftab = ".tabcontainer "+deftab;
		jQuery(tabcondeftab).show();
	}else{
		jQuery("ul.tabs li:first").addClass("active").show(); /* Activate first tab */
		jQuery(".tab-content:first").show(); /* Show first tab content */
	}
	/* On Click Event */
	jQuery("ul.tabs li").click(function() {
		jQuery("ul.tabs li").removeClass("active"); /* Remove any "active" class */
		jQuery(this).addClass("active"); /* Add "active" class to selected tab */
		jQuery(".tab-content").hide(); /* Hide all tab content */
		var activeTab = jQuery(this).find("a").attr("href"); /* Find the rel attribute value to identify the active tab + content */
		jQuery(activeTab).fadeIn(200); /* Fade in the active content */
		return false;
	});
}

function toggle_menu(){
	"use strict";
	jQuery('a.nav-toggle').click(function(evt){
		var outerheader = jQuery('#outerheader');
		var outerheaderinnerh = outerheader.innerHeight();
		var topnavpos = outerheaderinnerh ;
		
		var topnav = jQuery('.topnav');
		var video = jQuery('video.video');
		
		topnav.css('top', topnavpos);
		topnav.slideToggle('slow',function(){
			if(isMobile()){
				if( topnav.css('display')=='block' ){
					video.addClass('hidden');
				}else{
					video.removeClass('hidden');
				}
			}
		});
		
		jQuery('.topnav li a').click(function(){
			topnav.slideUp('slow');
			video.removeClass('hidden');
		});
		return false;
	});
}

function show_toggle(){
	"use strict";
	/*jQuery toggle*/
	var togtrigger = jQuery("h2.trigger");
	
	jQuery(".toggle_container").hide();
	var isiPhone = /iphone/i.test(navigator.userAgent.toLowerCase());
	if (isiPhone){
		togtrigger.click(function(){
			if( jQuery(this).hasClass("active")){
				jQuery(this).removeClass("active");
				jQuery(this).next().css('display','none');
			}else{
				jQuery(this).addClass("active");
				jQuery(this).next().css('display','block');
			}
			return false;
		});
	}else{
		togtrigger.click(function(){
			jQuery(this).toggleClass("active").next().slideToggle("slow");
			return false;
		});
	}
}

function counter_effect(){
	"use strict";
	//Animated Counters
	jQuery(".counters").appear(function() {
	var counter = jQuery(this).html();
	jQuery(this).countTo({
		from: 0,
		to: counter,
		speed: 2000,
		refreshInterval: 60,
		});
	});
}

function portfolio_effect(){
	"use strict";
	
	var pfbox = jQuery('.nvr-pf-container li .nvr-pf-box, .flexslider-carousel li .nvr-pf-box, div.frameimg');
	/*=================================== FADE EFFECT ===================================*/
	pfbox.mouseenter(function(){
		var pfthis = jQuery(this);
		pfthis.find('div.rollover').stop().animate({"opacity":0.75}, 500, 'easeOutCubic');
		pfthis.find('a.image').stop().animate({"left":"50%","opacity":0.90}, 500, 'easeOutCubic');
		pfthis.find('a.zoom').stop().animate({"left":"50%","opacity":0.90}, 500, 'easeOutCubic');
	});
	pfbox.mouseleave(function(){
		var pfthis = jQuery(this);
		pfthis.find('div.rollover').stop().animate({"opacity":0}, 500, 'easeOutCubic');
		pfthis.find('a.image').stop().animate({"left":"-10%","opacity":0}, 500, 'easeOutCubic');
		pfthis.find('a.zoom').stop().animate({"left":"110%","opacity":0}, 500, 'easeOutCubic');
	});
}

function portfolioajax(){
	
	jQuery('a.pfajax').click(function(evt){
		
		evt.preventDefault();
		
		var pfitem = jQuery(this);
		var pfurl = jQuery(this).attr('href');
		var pfcontainer = jQuery(this).parents('.nvr-pf-container');
		var ajaxbutton = pfcontainer.find('a.btnajax');
		var pfbottom = pfcontainer.find('.pfbottom');
		var pajaxholder = pfcontainer.find('div.portfolio-ajax-holder');
		var pajaxdata = pajaxholder.find('div.portfolio-ajax-data');
		
		var cactive = false;
		var loadedimages = 0;
		var loadedpercent = 0;
		var outerheader = jQuery('#outerheader');
		var wpadminbar = jQuery('#wpadminbar');
		
		var adminbarinnerh = wpadminbar.innerHeight();
		var outerheaderinnerh = outerheader.innerHeight();
		var topscrolledge = adminbarinnerh+outerheaderinnerh;
		
		pajaxholder.removeClass('preloader');
		
		if(pfitem.hasClass('active')){
			
		}else{
		
			pfitem.addClass('active');
			
			if(jQuery('body').hasClass('nvronepage')){
			
				jQuery('html, body').animate({ scrollTop: pfbottom.offset().top - topscrolledge }, 900);
				
				ajaxbutton.click(function(){
					pajaxdata.animate({opacity:0}, 400,function(){
						
						pajaxholder.delay(400).slideUp(400, function(){
							pajaxdata.empty();
						});
						
					});
						
					jQuery('html, body').delay(1000).animate({ scrollTop: pfcontainer.offset().top - topscrolledge}, 800);
					pfitem.removeClass('active') ;
				  
					return false;
				});
				
				pajaxholder.slideUp(600, function(){ 
					pajaxdata.css('visibility', 'visible');
					
					var jqxhr = jQuery.ajax({
						url : pfurl,
						cache : false,
						dataType : 'html',
						async : true,
						beforeSend : function(){
							pajaxdata.empty();
						},
						xhr: function(){
							var xhr = new window.XMLHttpRequest();
							/*Upload progress*/
							xhr.upload.addEventListener("progress", function(evt){
								if (evt.lengthComputable) {
									var percentComplete = evt.loaded / evt.total;
									/*Do something with upload progress*/
									console.log(percentComplete);
								}
							}, false);
							/*Download progress*/
							xhr.addEventListener("progress", function(evt){
								if (evt.lengthComputable) {
									var percentComplete = Math.round((evt.loaded/evt.total)*100);
									/*Do something with download progress*/
									console.log(percentComplete);
								}
							}, false);
							return xhr;
						}
					});
					
					jqxhr.done(function(data, textStatus){
						
						var content = data;
						
						pajaxdata.append(content);
						pajaxholder.slideDown(1000,function(){
							pajaxholder.addClass('preloader');
							
							pajaxdata.imagesLoaded()
							.always( function( instance ) {
								console.log('loading images');
							})
							.done( function( instance ) {
								slider_init();
									
									pajaxdata.delay(1000).animate({opacity:1},900,function(){ pajaxholder.removeClass('preloader'); });
				
									jQuery('.element_fade_in').each(function () {
										jQuery(this).appear(function() {
											jQuery(this).delay(100).animate({opacity:1,right:"0px"},1000);
										});
									});
								
								
							})
							.fail( function() {
								console.log('all images loaded, at least one is broken');
							})
							.progress( function( instance, image ) {
								var totalimage = instance.images.length;
								var result = image.isLoaded ? 'loaded' : 'broken';
								if(result=='loaded'){
									loadedimages++;
								}
								loadedpercent = Math.round((loadedimages/totalimage)*100);
								console.log( 'image is ' + result + ' for ' + image.img.src );
							});
						});
				
					});
					
					jqxhr.fail(function(error, textStatus){
						alert( "Request failed: " + textStatus );
					});
				
				});
			
			}else if(jQuery('body').hasClass('nvrflippage')){
				
				pajaxholder.appendTo('#subbody');
				ajaxbutton.click(function(){
						
					pajaxholder.delay(400).fadeOut(600, function(){
						pajaxdata.empty();
						pajaxholder.perfectScrollbar('destroy');
						pajaxholder.appendTo(pfcontainer);
					});
					
					pfitem.removeClass('active') ;
				  
					return false;
				});
				
				pajaxholder.fadeIn(600, function(){ 
					pajaxdata.css('visibility', 'visible');
					pajaxdata.fadeOut(100);
					pajaxholder.addClass('preloader');
					
					var jqxhr = jQuery.ajax({
						url : pfurl,
						cache : false,
						dataType : 'html',
						async : true,
						beforeSend : function(){
							pajaxdata.empty();
						},
						xhr: function(){
							var xhr = new window.XMLHttpRequest();
							/*Upload progress*/
							xhr.upload.addEventListener("progress", function(evt){
								if (evt.lengthComputable) {
									var percentComplete = evt.loaded / evt.total;
									/*Do something with upload progress*/
									console.log(percentComplete);
								}
							}, false);
							/*Download progress*/
							xhr.addEventListener("progress", function(evt){
								if (evt.lengthComputable) {
									var percentComplete = Math.round((evt.loaded/evt.total)*100);
									/*Do something with download progress*/
									console.log(percentComplete);
								}
							}, false);
							return xhr;
						}
					});
					
					jqxhr.done(function(data, textStatus){
						
						var content = data;
						
						pajaxdata.append(content);
							
						pajaxdata.imagesLoaded()
						.always( function( instance ) {
							console.log('loading images');
						})
						.done( function( instance ) {
							pajaxholder.perfectScrollbar({includePadding : true});
							pajaxdata.delay(1000).fadeIn(900,function(){ 
								pajaxholder.removeClass('preloader'); 
								pajaxholder.scrollTop(0);
								pajaxholder.perfectScrollbar('update');
								slider_init();
							});
		
							jQuery('.element_fade_in').each(function () {
								jQuery(this).appear(function() {
									jQuery(this).delay(100).animate({opacity:1,right:"0px"},1000);
								});
							});
							
						})
						.fail( function() {
							console.log('all images loaded, at least one is broken');
						})
						.progress( function( instance, image ) {
							var totalimage = instance.images.length;
							var result = image.isLoaded ? 'loaded' : 'broken';
							if(result=='loaded'){
								loadedimages++;
							}
							loadedpercent = Math.round((loadedimages/totalimage)*100);
							console.log( 'image is ' + result + ' for ' + image.img.src );
						});
				
					});
					
					jqxhr.fail(function(error, textStatus){
						alert( "Request failed: " + textStatus );
					});
				
				});
				
			}
		
		}
		
		return false;
	
	});
}

function show_socialicon(){
	"use strict";
	/*=================================== SOCIAL ICON ===================================*/
	var socialicons = jQuery('ul.sn li a');
	socialicons.hover(
		function() {
			var iconimg = jQuery(this).children();
			iconimg.stop(true,true);
			iconimg.fadeOut(500);
		},
		function() {
			var iconimg = jQuery(this).children();
			iconimg.stop(true,true);
			iconimg.fadeIn(500);
		}
	);
	
	if (window.devicePixelRatio > 1) {

		socialicons.each(function(i) {
			var lowres = jQuery(this).css('background-image');
			var highres = lowres.replace(".png", "@2x.png");
			jQuery(this).css('background-image', highres);
		});
	}
}

function show_lightbox(){
	"use strict";
	var lbrel = jQuery('a[data-rel]');
	
	/*=================================== PRETTYPHOTO ===================================*/
	lbrel.each(function() {jQuery(this).attr('rel', jQuery(this).data('rel'));});
	jQuery("a[rel^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',gallery_markup:'',slideshow:2000});
}

function show_carousel(){
	"use strict";
	var ctype = {
		"pcarousel" : {
			"index" : '.pcarousel .flexslider-carousel, .postcarousel .flexslider-carousel',
			"minItems" : 2,
			"maxItems" : 4,
			"itemWidth" : 197
		},
		"bcarousel" : {
			"index" : '.brand .flexslider-carousel',
			"minItems" : 2,
			"maxItems" : 5,
			"itemWidth" : 197
		}
	}
	
	for(var key in ctype){
		var carousel = ctype[key];
		jQuery(carousel.index).flexslider({
			animation: "slide",
			animationLoop: true,
			directionNav: true,
			controlNav: false,
			itemWidth: carousel.itemWidth,
			itemMargin: 0,
			prevText : '',
			nextText : '',
			minItems: carousel.minItems,
			maxItems: carousel.maxItems
		 });
	}
}

function slider_init(){
	"use strict";
	var slidereffect 			= interfeis_var.slidereffect;
    var slider_interval 		= interfeis_var.slider_interval;
    var slider_disable_nav 		= interfeis_var.slider_disable_nav;
    var slider_disable_prevnext	= interfeis_var.slider_disable_prevnext;
	
	var slideitems = jQuery('.slideritems');
	var slidesli = jQuery('.section.slider ul.slides > li');
	var theslider = jQuery('.flexslider');
	
    if(slider_disable_prevnext=="0"){
        var direction_nav = true;
    }else{
        var direction_nav = false;
    }
    
    if(slider_disable_nav=="0"){
        var control_nav = true;
    }else{
        var control_nav = false;
    }

    theslider.not('.nvr-trotating').not('.minisliders').flexslider({
        animation: slidereffect,
        slideshowSpeed: slider_interval,
        directionNav: direction_nav,
        controlNav: control_nav,
        smoothHeight: false,
		pauseOnHover: true,
		prevText : '',
		nextText : '',
		start : function(){
			slideitems.removeClass('preloader');
			slidesli.filter('.flex-active-slide').children('.flex-caption').addClass('flip');
			slidesli.not('.flex-active-slide').children('.flex-caption').removeClass('flip');
		},
		after : function(slider){
			slidesli.filter('.flex-active-slide').children('.flex-caption').addClass('flip');
			slidesli.not('.flex-active-slide').children('.flex-caption').removeClass('flip');
		}
    });
	
	theslider.filter('.nvr-trotating').flexslider({
        animation: 'fade',
        slideshowSpeed: slider_interval,
        directionNav: direction_nav,
        controlNav: control_nav,
        smoothHeight: false,
		pauseOnHover: true,
		prevText : '',
		nextText : ''
    });
	
	theslider.filter('.minisliders').flexslider({
        animation: slidereffect,
        slideshowSpeed: slider_interval,
        directionNav: direction_nav,
        controlNav: control_nav,
        smoothHeight: true,
		pauseOnHover: true,
		prevText : '',
		nextText : ''
    });
}

function calculate_sliderheight(){
	"use strict";
	var windowheight 			= jQuery(window).height();
	var wpadminbarh				= jQuery('#wpadminbar').innerHeight();
    jQuery('.section.slider .flexslider ul.slides li').css('height',windowheight-wpadminbarh);
}

function calculate_videoheight(){
	"use strict";
	var windowheight 			= jQuery(window).height();
	var wpadminbarh				= jQuery('#wpadminbar').innerHeight();
    jQuery('.section.slider .video-wrap').css('height',windowheight-wpadminbarh);
}

function isotopeinit(){
	"use strict";
	
	var pffilter = jQuery('#nvr-pf-filter');
    pffilter.isotope({
        itemSelector : '.element'
    });
	
	pffilter.infinitescroll({
		loading: {
			finishedMsg: 'ALL POSTS LOADED',
			msg: null,
			msgText: 'LOADING NEXT POSTS',
			img: interfeis_var.themeurl + 'images/pf-loader.gif'
		  },
			navSelector  : '#loadmore-paging',    // selector for the paged navigation 
			nextSelector : '#loadmore-paging .loadmorebutton a:first',  // selector for the NEXT link (to page 2)
			itemSelector : '.element',     // selector for all items you'll retrieve
			bufferPx: 40
		},
       	// call Isotope as a callback
		function ( newElements ) {

			var $newElems = jQuery( newElements ).css({ opacity: 0 });
			$newElems.imagesLoaded(function(){
				$newElems.animate({ opacity: 1 });
				pffilter.isotope( 'appended', $newElems, true );
				pffilter.isotope('reLayout');
				portfolio_effect();
				show_lightbox();
			});
		}
	);
	
	var filtersli = jQuery('#filters li');
	filtersli.click(function(){
        filtersli.removeClass('selected');
        jQuery(this).addClass('selected');
        var selector = jQuery(this).find('a').attr('data-option-value');
        pffilter.isotope({ filter: selector });
        return false;
    });
	
	var postisotope = jQuery('.postscontainer.mason').isotope({
		itemSelector : '.articlewrapper'
	});
	
	postisotope.infinitescroll({
		loading: {
			finishedMsg: 'ALL POSTS LOADED',
			msg: null,
			msgText: 'LOADING NEXT POSTS',
			img: interfeis_var.themeurl + 'images/pf-loader.gif'
		  },
			navSelector  : '#loadmore-paging',    // selector for the paged navigation 
			nextSelector : '#loadmore-paging .loadmorebutton a:first',  // selector for the NEXT link (to page 2)
			itemSelector : '.articlewrapper',     // selector for all items you'll retrieve
			bufferPx: 40
		},
       	// call Isotope as a callback
		function ( newElements ) {
			
			slider_init();

			var $newElems = jQuery( newElements ).css({ opacity: 0 });
			$newElems.imagesLoaded(function(){
				$newElems.animate({ opacity: 1 });
				postisotope.isotope( 'appended', $newElems, true );
				postisotope.isotope('reLayout');
			});
		}
	);
	
	
	jQuery('#loadmore-paging a:first').click(function(evt){
		evt.preventDefault();
		jQuery(document).trigger('retrieve.infscr');
		return false;
	});
	jQuery(document).ajaxError(function(e,xhr,opt){
		if(xhr.status==404){jQuery('#loadmore-paging a').remove();}
	});
}

function form_styling(){
	"use strict";
	/* Select */
	var selects = jQuery('select#cmbpostdomain');
	selects.wrap('<div class="nvr_selector" />');
	var selector = jQuery('.nvr_selector');
	selector.prepend('<span />');
	selector.each(function(){
		var selval = jQuery(this).find('select option:selected').text();
		var sel = jQuery(this).children('select');
		var selclass = sel.attr('class');
		jQuery(this).children('span').text(selval);
		jQuery(this).addClass(selclass);
		sel.css('width','100%');
		sel.change(function(){
			var selvals = jQuery(this).children('option:selected').text();
			jQuery(this).parent().children('span').text(selvals);
		});
	});
}

function appear_effect(){
	"use strict";
	
	var elemtop = jQuery('.element_from_top');
	var elembottom = jQuery('.element_from_bottom');
	var elemleft = jQuery('.element_from_left');
	var elemright = jQuery('.element_from_right');
	var elemfade = jQuery('.element_fade_in');
	
	/* Elements Fading */
	elemtop.each(function () {
		jQuery(this).appear(function() {
		  jQuery(this).delay(150).animate({opacity:1,top:"0px"},1000);
		});	
	});
	
	elembottom.each(function () {
		jQuery(this).appear(function() {
		  jQuery(this).delay(150).animate({opacity:1,bottom:"0px"},1000);
		});	
	});
	
	
	elemleft.each(function () {
		jQuery(this).appear(function() {
		  jQuery(this).delay(150).animate({opacity:1,left:"0px"},1000);
		});	
	});
	
	
	elemright.each(function () {
		jQuery(this).appear(function() {
		  jQuery(this).delay(150).animate({opacity:1,right:"0px"},1000);
		});	
	});
		
	elemfade.each(function () {
		jQuery(this).appear(function() {
		  jQuery(this).delay(150).animate({opacity:1,right:"0px"},1000);
		});	
	});
}

/*=================================== TOPCART ==============================*/

function topsearch_effects(){
	"use strict";
	
	var btnsearch = jQuery('.searchbox .submit');
	var searcharea = jQuery('.searchbox .searcharea');
	var textsearch = jQuery('.searchbox .txtsearch');
	
	btnsearch.on('click', function(evt){
		if(textsearch.val()==''){
			evt.preventDefault();
			if(searcharea.hasClass('shown')){
				searcharea.removeClass('shown');
				searcharea.fadeOut();
			}else{
				searcharea.addClass('shown');
				searcharea.fadeIn();
			}
		}
	});
}

var Page = (function() {

	var $container = jQuery( '#outermain' ),
		$bookBlock = jQuery( '#bb-bookblock' ),
		scrollelem = '.section.default',
		$items = $bookBlock.children(),
		itemsCount = $items.length,
		current = 0,
		bb = jQuery( '#bb-bookblock' ).bookblock( {
			speed : 800,
			perspective : 2000,
			shadowSides	: 0.8,
			shadowFlip	: 0.4,
			onEndFlip : function(old, page, isLimit) {
				
				current = page;
				// update TOC current
				updateMenu();
				// updateNavigation
				updateNavigation( isLimit );
				
				updateMenuColor();
				// initialize jScrollPane on the content div for the new item
				stickyheader();
				
				if($items.eq( current ).find( '.gmapswrapper' ).length){
					show_gmaps();
				}
				
				/* still testing
				jQuery('video.video').each(function(){
					jQuery(this).get(0).pause();
				});
				var $sectionslider = $items.eq( current ).children( '.section.slider' );
				$sectionslider.find('video.video').each(function(){
					jQuery(this).get(0).play();
				});
				*/
				
				
				jQuery('.pcarousel .flexslider-carousel, .postcarousel .flexslider-carousel, .brand .flexslider-carousel').removeData("flexslider");
				show_carousel();
				isotopeinit();
				setJSP( 'init' );
				setTimeout(function(){updateJSP();},1700);
			}
		} ),
		$navNext = jQuery( '#bb-nav-next' ),
		$navPrev = jQuery( '#bb-nav-prev' ).hide(),
		$menuItems = jQuery( '.nvrflippage ul.sf-menu > li' ),
		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[Modernizr.prefixed('transition')],
		supportTransitions = Modernizr.csstransitions;

	function init() {
		
		setJSP( 'init' );
		initEvents();
		stickyheader();
		hide_gmaps();
		var pageid = jQuery(window.location.hash);
		var idx = jQuery('.bb-item').index(pageid);
		bb.jump( idx + 1 );
		
	}
	
	function hide_gmaps(){
		"use strict";
		
		jQuery('.nvrflippage iframe.gmaps').each(function(idx){
			var srciframe = jQuery(this).attr('src');
			var hiframe = jQuery(this).height();
			jQuery(this).wrap('<div class="gmapswrapper"></div>');
			var gmapswrap = jQuery('.gmapswrapper');
			gmapswrap.data('mapsrc',srciframe);
			gmapswrap.data('height',hiframe);
			gmapswrap.css('height',hiframe);
			jQuery(this).remove();
		});
	}
	
	function show_gmaps(){
		"use strict";
		
		jQuery('.nvrflippage .gmapswrapper').each(function(idx){
			if(!jQuery(this).children('.gmaps').length){
				var framesrc = jQuery(this).data('mapsrc');
				var hiframe = jQuery(this).data('height');
				jQuery(this).append('<iframe class="gmaps"></iframe>');
				jQuery(this).children('.gmaps').attr({
					'src' : framesrc,
					'height' : hiframe,
					'frameborder' : 0,
					'width' : '100%'
				});
			}
		});
	}
	
	function stickyheader(){
		var outerheader = jQuery('#outerheader');
		var $content = $items.eq( current ).children( '.section.default' );
		
		jQuery($content).scroll(function(evt){
			
			var scrolltop = jQuery($content).scrollTop();
			
			if(scrolltop>0){
				outerheader.addClass("sticky");
			}else{
				outerheader.removeClass("sticky");
			}
		});
	}
	
	function setJSP( action, idx ) {
		jQuery(scrollelem).perfectScrollbar({	includePadding : true});
		jQuery(scrollelem).scrollTop(0);
		jQuery(scrollelem).perfectScrollbar('update');
		
	}
	
	function updateJSP() {
		jQuery(scrollelem).perfectScrollbar('update');
	}
	
	function initEvents() {

		// add navigation events
		$navNext.on( 'click', function() {
			bb.next();
			return false;
		} );

		$navPrev.on( 'click', function() {
			bb.prev();
			return false;
		} );
		
		// add swipe events
		$items.on( {
			'swipeleft'		: function( event ) {
				if( $container.data( 'opened' ) ) {
					return false;
				}
				bb.next();
				return false;
			},
			'swiperight'	: function( event ) {
				if( $container.data( 'opened' ) ) {
					return false;
				}
				bb.prev();
				return false;
			}
		} );

		// click a menu item
		$menuItems.on( 'click', function(evt) {
			
			var $el = jQuery( this ).children('a');
			var	idx = $el.index();
			var pageid = jQuery($el.attr('href'));
			var idx = jQuery('.bb-item').index(pageid);
			
			window.location.hash = $el.attr('href');
			var $content = $items.eq( current ).children( '.section.default' );
			
			if($content.attr('class')===undefined || $content.scrollTop()==0){
				bb.jump( idx + 1 );
			}else{
				$content.stop().animate({
					scrollTop: 0
				}, 500,'easeInOutExpo',function(evt){
					bb.jump( idx + 1 );
				});
			}
			
			return false;
			
		} );
		
		jQuery('a.sliderbutton, a.linked').on( 'click', function(evt) {
			
			var $el = jQuery( this );
			if(jQuery($el.attr('href')).length){
				var	idx = $el.index();
				var pageid = jQuery($el.attr('href'));
				var idx = jQuery('.bb-item').index(pageid);
				
				window.location.hash = $el.attr('href');
				var $content = $items.eq( current ).children( '.section.default' );
				
				if($content.attr('class')===undefined || $content.scrollTop()==0){
					bb.jump( idx + 1 );
				}else{
					$content.stop().animate({
						scrollTop: 0
					}, 500,'easeInOutExpo',function(evt){
						bb.jump( idx + 1 );
					});
				}
			}
			
			return false;
			
		} );
		
		// reinit jScrollPane on window resize
		jQuery( window ).on( 'debouncedresize', function() {
			setJSP( 'reinit' );
		} );

	}
	
	function updateMenuColor(){
		var $content = $items.eq( current ).children( '.section' );
		var menucolor = $content.hasClass('dark-menu')? 'dark-menu' : 'light-menu';
		
		jQuery('body.nvrflippage').removeClass('dark-menu light-menu').addClass(menucolor);
	}


	function updateMenu() {
		
		$menuItems.removeClass( 'current-menu-item' ).eq( current ).addClass( 'current-menu-item' );
	}

	function updateNavigation( isLastPage ) {
		
		if( current === 0 ) {
			$navNext.show();
			$navPrev.hide();
		}
		else if( isLastPage ) {
			$navNext.hide();
			$navPrev.show();
		}
		else {
			$navNext.show();
			$navPrev.show();
		}

	}

	return { init : init };

})();